package masishta.parsig;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Masishta on 1/27/2016.
 */
public class ParsigInfoHolder implements Parcelable
{
    private int identifier;
    private int pIdentifier;
    private String desc;
    private String en;
    private String de;
    private String fr;
    private String refs;
    private String pes;

    /* ParsigInfoHolder Constructor */
    public ParsigInfoHolder(int id, int pid, String description, String english,
                            String deutsch, String french, String reference, String persian)
    {
        identifier  = id;
        pIdentifier = pid;
        desc        = description;
        en          = english;
        de          = deutsch;
        fr          = french;
        refs        = reference;
        pes         = persian;
    }

    public int getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(int identifier)
    {
        this.identifier = identifier;
    }

    public int getpIdentifier()
    {
        return pIdentifier;
    }

    public void setpIdentifier(int pIdentifier)
    {
        this.pIdentifier = pIdentifier;
    }

    public String getPes()
    {
        return pes;
    }

    public void setPes(String pes)
    {
        this.pes = pes;
    }

    public String getDesc()
    {
        return desc;
    }

    public void setDesc(String desc)
    {
        this.desc = desc;
    }

    public String getEn()
    {
        return en;
    }

    public void setEn(String en)
    {
        this.en = en;
    }

    public String getDe()
    {
        return de;
    }

    public void setDe(String de)
    {
        this.de = de;
    }

    public String getFr()
    {
        return fr;
    }

    public void setFr(String fr)
    {
        this.fr = fr;
    }

    public String getRefs()
    {
        return refs;
    }

    public void setRefs(String refs)
    {
        this.refs = refs;
    }

    private ParsigInfoHolder(Parcel in) {
        desc = in.readString();
        en = in.readString();
        de = in.readString();
        fr = in.readString();
        refs = in.readString();
        pes = in.readString();
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(desc);
        out.writeString(en);
        out.writeString(de);
        out.writeString(fr);
        out.writeString(refs);
        out.writeString(pes);
    }

    public int describeContents() {
        return 0;
    }

    // Just cut and paste this for now
    public static final Parcelable.Creator<ParsigInfoHolder> CREATOR = new Parcelable.Creator<ParsigInfoHolder>() {
        public ParsigInfoHolder createFromParcel(Parcel in) {
            return new ParsigInfoHolder(in);
        }

        public ParsigInfoHolder[] newArray(int size) {
            return new ParsigInfoHolder[size];
        }
    };
}

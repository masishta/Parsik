package masishta.parsig;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity
{
    private EditText edittext;
    private TextView errorView;
    Button button;
    String tag = "Main";
    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DatabaseHandler(this);
        try
        {
            db.createDataBase(this);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        edittext = (EditText) findViewById(R.id.editText);
        searchFilterListener();
        errorView = (TextView) findViewById(R.id.errorView);
        button = (Button) findViewById(R.id.button);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    String getKeyword(EditText key)
    {
        Log.d(tag, "getKeyword");
        String keyword = key.getText().toString();
        if (!checkString(keyword))
            return keyword;
        return "error";
    }

    Boolean checkString(String key)
    {
        Log.d(tag, "checkString");
        Pattern pattern = Pattern.compile("[~#@$*+%&{}<>\\[\\]|\"\'/;.,:()!?؟«»_^]");
        Matcher matcher = pattern.matcher(key);
        return matcher.find();
    }

    public void searchResult(View view)
    {
        String keyword = getKeyword(edittext);
        Intent intent = new Intent(this, SearchResult.class);
        intent.putExtra("search_key", keyword);
        startActivity(intent);
    }
/*    private class buttonClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            Log.e(tag, "onClick");
            String key = getKeyword(edittext);
            List<ParsigInfoHolder> enResult = db.enSearchResult(key);
            for(ParsigInfoHolder i:enResult)
            {
                Log.e("Result: ", "en:" + i.getEn() + " | Fa:" + i.getPes() + " | Desc:" + i.getDesc());
            }
            Toast.makeText(MainActivity.this, key + " result count: " + enResult.size(), Toast.LENGTH_SHORT).show();
        }
    }*/

    public void searchFilterListener()
    {
        edittext.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                Log.d(tag, "onTextChanged");
                if (checkString(s.toString()))
                {
                    errorView.setText(R.string.search_error);
                    button.setEnabled(false);
                }
                else
                {
                    errorView.setText("");
                    button.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });
    }


    @Override
    public void onBackPressed()
    {

    }
}

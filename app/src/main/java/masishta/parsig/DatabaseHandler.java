package masishta.parsig;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper
{
    // Database Version
    static final int DATABASE_VERSION = 1;

    // Database Name
    static final String DATABASE_NAME = "paarsig.sqlite";

    // Database Path
    String DATABASE_PATH;

    // Database Asset Path
    String DATABASE_ASSETS_PATH = "paarsig.sqlite";

    // Table Name
    final String TABLE_NAME = "parsik_senses";

    // Parsig Table Columns Names
    private static final String _ID = "ID";
    private static final String _PERSIAN = "name";
    private static final String _DESCRIPTION = "desc";
    private static final String _ENGLISH = "en";
    private static final String _DEUTSCH = "de";
    private static final String _FRENCH = "fr";
    private static final String _REFERENCE = "refs";


    public DatabaseHandler(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.e("FINE","Constructor");

        // Check Database
        if(android.os.Build.VERSION.SDK_INT >= 17)
        {
          DATABASE_PATH = context.getApplicationInfo().dataDir + "/databases/";
        }
        else
        {
            DATABASE_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }

    }

    // Creating Table
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        Log.e("onCreate", "onCreate");
      /*db.rawQuery ("create table BIODATA(_id integer primary key autoincrement, "
                           + "code text not null,"
                           + "name text not null"
                           +");"
      , null);*/
    }

    // Upgrading Database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }

    public void createDataBase(Context ctx) throws IOException
    {
        Log.e("FINE", "createDataBase");
        if(!checkDataBase())
        {
            this.getReadableDatabase();
            copyDatabase(ctx);
        }
    }

    private boolean checkDataBase()
    {
        Log.e("FINE","checkDataBase");
        SQLiteDatabase checkDB = null;
        try
        {
            String myPath = DATABASE_PATH + DATABASE_NAME;
            Log.e("MyPath", "mypath: " + myPath);
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        }
        catch(Exception e)
        {
            //database does't exist yet.
            e.printStackTrace();
        }

        if(checkDB != null)
        {
            checkDB.close();
        }

        return checkDB != null;
    }

    private void copyDatabase(Context ctx) throws IOException
    {
        Log.e("FINE", "copyDatabase");
        InputStream myInput = ctx.getAssets().open(DATABASE_ASSETS_PATH);
        String outFileName = DATABASE_PATH + DATABASE_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;

        while ((length = myInput.read(buffer))>0)
        {
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    // Return Search Result
    public ArrayList<ParsigInfoHolder> searchResult(String key)
    {
        ArrayList<ParsigInfoHolder> infoList = new ArrayList<ParsigInfoHolder>();

        // Select All Query
        String selectQuery = "SELECT DISTINCT * FROM " + TABLE_NAME +
                " WHERE desc LIKE '%" + key + "%' " +
                "OR en LIKE '%" + key + "%' " +
                "OR de LIKE '%" + key + "%' " +
                "OR fr LIKE '%" + key + "%' " +
                "OR pes LIKE '%" + key + "%' " +
                "ORDER BY ID DESC;";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null)
        {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext())
            {
                ParsigInfoHolder info = new ParsigInfoHolder(cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7));

                infoList.add(info);
            }
        }



        return infoList;
    }

    public List<ParsigInfoHolder> enSearchResult(String key)
    {
        List<ParsigInfoHolder> infoList = new ArrayList<ParsigInfoHolder>();

        // Select All Query
        String selectQuery = "SELECT DISTINCT * FROM " + TABLE_NAME +
                " WHERE en LIKE '%" + key + "%' " +
                "ORDER BY ID DESC;";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null)
        {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext())
            {
                ParsigInfoHolder info = new ParsigInfoHolder(cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7));

                infoList.add(info);
            }
        }



        return infoList;
    }

    public List<ParsigInfoHolder> perSearchResult(String key)
    {
        List<ParsigInfoHolder> infoList = new ArrayList<ParsigInfoHolder>();

        // Select All Query
        String selectQuery = "SELECT DISTINCT * FROM " + TABLE_NAME +
                " WHERE pes LIKE '%" + key + "%' " +
                "ORDER BY ID DESC;";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null)
        {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext())
            {
                ParsigInfoHolder info = new ParsigInfoHolder(cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7));

                infoList.add(info);
            }
        }



        return infoList;
    }

}

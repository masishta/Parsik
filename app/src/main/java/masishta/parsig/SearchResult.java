package masishta.parsig;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class SearchResult extends AppCompatActivity
{
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_result);
        final String key = getIntent().getStringExtra("search_key");
        DatabaseHandler db = new DatabaseHandler(this);
        final ArrayList<ParsigInfoHolder> result = db.searchResult(key);
        SearchResultAdapter adapter = new SearchResultAdapter(this, result);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent intent = new Intent(SearchResult.this, SearchInfo.class);
                intent.putExtra("result", result.get(position));
                startActivity(intent);
            }
        });
    }

}

package masishta.parsig;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class SearchResultAdapter extends ArrayAdapter<ParsigInfoHolder>
{
    TextView desc;
    TextView en;
    TextView pes;
//    TextView de;
//    TextView fr;
//    TextView refs;

    public SearchResultAdapter(Context context, ArrayList<ParsigInfoHolder> info)
    {
        super(context, 0, info);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.search_info, parent, false);

        pes = (TextView) convertView.findViewById(R.id.pes);
        desc = (TextView) convertView.findViewById(R.id.desc);
        en = (TextView) convertView.findViewById(R.id.en);
//        de = (TextView) convertView.findViewById(R.id.de);
//        fr = (TextView) convertView.findViewById(R.id.fr);
//        refs = (TextView) convertView.findViewById(R.id.refs);

        pes.setText(getItem(position).getPes());
        desc.setText(getItem(position).getDesc());
        en.setText(getItem(position).getEn());
//        de.setText(getItem(position).getDe());
//        fr.setText(getItem(position).getFr());
//        refs.setText(getItem(position).getRefs());

        return convertView;
    }
}


package masishta.parsig;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;


/**
 * Created by Masishta on 10/2/2016.
 */

public class SearchInfo extends AppCompatActivity
{
    TextView pesTitleView;
    TextView pesView;
    TextView descTitleView;
    TextView descView;
    TextView enTitleView;
    TextView enView;
    TextView deView;
    TextView frView;
    TextView refsView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_info);
        ParsigInfoHolder info = getIntent().getParcelableExtra("result");

        pesTitleView = (TextView) findViewById(R.id.pesTitle);
        pesView = (TextView) findViewById(R.id.pes);
        pesView.setText(info.getPes());

        descTitleView = (TextView) findViewById(R.id.descTitle);
        descView = (TextView)findViewById(R.id.desc);
        descView.setText(info.getDesc());

        enTitleView = (TextView) findViewById(R.id.enTitle);
        enView = (TextView) findViewById(R.id.en);
        enView.setText(info.getEn());

        deView = (TextView) findViewById(R.id.de);
        deView.setText(info.getDe());

        frView = (TextView) findViewById(R.id.fr);
        if(info.getFr() != null && !info.getFr().equals(""))
            frView.setText(info.getFr());
        else
            frView.setVisibility(View.GONE);


        refsView = (TextView) findViewById(R.id.refs);
        refsView.setText(info.getRefs());
    }
}
